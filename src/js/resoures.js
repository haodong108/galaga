/**资源管理器*/
var resoures = {
    img_gongfeng_f1: Tools.createImg("./img/yy/gf_f1.png"),
    img_gongfeng_f2: Tools.createImg("./img/yy/gf_f2.png"),
    img_gongfeng_f3: Tools.createImg("./img/yy/gf_f3.png"),
    img_bingfeng_f1: Tools.createImg("./img/yy/bf_f1.png"),
    img_bingfeng_f2: Tools.createImg("./img/yy/bf_f2.png"),
    img_hero: Tools.createImg("./img/yy/hero_s1_f1.png"),
    img_hero_red: Tools.createImg("./img/yy/hero_s2_f1.png"),
    img_fengwang_s1_f1: Tools.createImg("./img/yy/fw_s1_f1.png"),
    img_fengwang_s1_f2: Tools.createImg("./img/yy/fw_s1_f2.png"),
    img_fengwang_s2_f1: Tools.createImg("./img/yy/fw_s2_f1.png"),
    img_fengwang_s2_f2: Tools.createImg("./img/yy/fw_s2_f2.png"),
    img_fengwang_s3_f1: Tools.createImg("./img/yy/fw_s3_f1.png"),
    img_fengwang_s3_f2: Tools.createImg("./img/yy/fw_s3_f2.png"),
    img_huwei: Tools.createImg("./img/yy/hwf_f1.png"),
    img_fengchong_f1: Tools.createImg("./img/yy/fc_f1.png"),
    img_fengchong_f2: Tools.createImg("./img/yy/fc_f2.png"),
    img_foe_bom1: Tools.createImg("./img/yy/foe_bom1.png"),
    img_foe_bom2: Tools.createImg("./img/yy/foe_bom2.png"),
    img_foe_bom3: Tools.createImg("./img/yy/foe_bom3.png"),
    img_foe_bom4: Tools.createImg("./img/yy/foe_bom4.png"),
    img_foe_bom5: Tools.createImg("./img/yy/foe_bom5.png"),
    img_hero_bom1: Tools.createImg("./img/yy/hero_bom1.png"),
    img_hero_bom2: Tools.createImg("./img/yy/hero_bom2.png"),
    img_hero_bom3: Tools.createImg("./img/yy/hero_bom3.png"),
    img_hero_bom4: Tools.createImg("./img/yy/hero_bom4.png"),
    anim_repeat_bingfeng: function () {
      return [this.img_bingfeng_f1, this.img_bingfeng_f2];
    },
    anim_repeat_gongfeng: function () {
      return [this.img_gongfeng_f1, this.img_gongfeng_f2];
    },
    anim_repeat_fengwang_s1: function () {
      return [this.img_fengwang_s1_f1, this.img_fengwang_s1_f2];
    },
    anim_repeat_fengwang_s2: function () {
      return [this.img_fengwang_s2_f1, this.img_fengwang_s2_f2];
    },
    anim_repeat_fengwang_s3: function () {
      return [this.img_fengwang_s3_f1, this.img_fengwang_s3_f2];
    },
    anim_repeat_fengchong: function () {
      return [this.img_fengchong_f1, this.img_fengchong_f2];
    },
    anim_foeExplode: function () {
      return [this.img_foe_bom1, this.img_foe_bom2, this.img_foe_bom3, this.img_foe_bom4, this.img_foe_bom5];
    },
    anim_heroExplode: function () {
      return [this.img_hero_bom1, this.img_hero_bom2, this.img_hero_bom3, this.img_hero_bom4];
    },
  };