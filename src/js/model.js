/**矩形对象 */
class Rectangle {
  constructor(x, y, w, h) {
    this.X = x;
    this.Y = y;
    this.width = w;
    this.height = h;
  }
}

/**坐标对象 */
class Point {
  /**
   *
   * @param {number} x x坐标
   * @param {number} y y坐标
   */
  constructor(x, y) {
    this.x = x ? x : 0;
    this.y = y ? y : 0;
  }
}
