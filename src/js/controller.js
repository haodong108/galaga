class Controller {
  otsp = 200; //单位出场时间间隔
  unitMargin_right = 20; //单位右间距
  attackTimer = 0; //攻击行为计时器

  /**排列完毕后的回调 */
  arranged = new Function();

  /**@type {Hero[]} 英雄机集群 */
  heros = [];
  /**@type {Foe_BingFeng[]} 兵蜂集群 */
  bingfengs = [];
  /**@type {Foe_HuWeiFeng[]} 护卫峰集群 */
  hwfs = [];
  /**@type {Foe_GongFeng[]} 工蜂集群 */
  gongfengs = [];
  /**@type {Foe_FengWang[]} 蜂王集群 */
  fengwangs = [];

  constructor() {}

  /**启动入场 */
  startGame() {
    //添加英雄机
    var hero = new Hero();
    hero.x = gameCanvas.width / 2 - hero.width / 2;
    hero.y = gameCanvas.height - hero.height - 10;
    hero.addEventListener("disposing", () => heros.remove(this));
    this.heros.push(hero);

    console.log(window.heros);
    painter.addPaintElement(hero);
    var tis = this;
    //蜜蜂入场
    new Promise(function () {
      tis.beeEnter_lef();
    }).then(() => {
      console.log("左侧添加完毕");
      //tis.fengwangs[tis.fengwangs.length - 1];
    });
    new Promise(function () {
      tis.beeEnter_rig();
    }).then(() => {
      console.log("右侧添加完毕");
    });

    setTimeout(() => {
      tis.attackTimer = setInterval(tis.runAttack, 5000);
    }, 10000);
  }

  /**随机指定蜜蜂发起攻击*/
  runAttack() {}

  /**添加蜂王护卫 */
  addFengWangHw() {
    window.fengwangs[fengwangs.length - 1].onMoveComplete = function () {
      for (let i = 0; i < fengwangs.length; i++) {
        const fw = fengwangs[i];
        let hl = hwfs[hwfs.length - (i * 2 + 1)];
        let hr = hwfs[hwfs.length - (i * 2 + 2)];
        if (hl && !hl.isDispose) {
          fw.bindLeftGuard(hl);
        }
        if (hr && !hr.isDispose) {
          fw.bindRightGuard(hr);
        }
      }
    };
  }

  //计算左侧基距
  calcBaseLeft(len, w) {
    return cavW / 2 - (len * (w + this.unitMargin_right) - this.unitMargin_right) / 2;
  }

  //左侧入场蜜蜂
  async beeEnter_lef(resolve, reject) {
    let top_bf = 230; //顶部距离
    let lstart = new Point(10, -80);
    let lc1 = new Point(60, 500);
    let lc2 = new Point(cavW / 2 + 400, 460);
    //兵峰出场
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 12; j++) {
        let bee = new Foe_BingFeng();
        let ey = top_bf + i * (bee.height + 10);
        let ex = this.calcBaseLeft(12, bee.width) + j * (bee.width + this.unitMargin_right);
        let end = new Point(ex, ey);
        bee.originalLocation = end;
        let bzr = Tools.calcThreeBezierPoints(lstart, end, lc1, lc2);
        bee.setMovePath(bzr);
        window.painter.addPaintElement(bee);
        window.bingfengs.push(bee);
        await sleep(this.otsp);
      }
    }
    let top_fw = 60; //顶部距离
    //蜂王入场
    for (let i = 0; i < 1; i++) {
      for (let j = 0; j < 4; j++) {
        let bee = new Foe_FengWang();
        let ey = top_fw + i * (bee.height + 10);
        let ex = cavW / 2 - (4 * (bee.width + this.unitMargin_right + 40) - (this.unitMargin_right + 40)) / 2 + j * (bee.width + this.unitMargin_right + 40);
        let end = new Point(ex, ey);
        bee.originalLocation = end;
        let bzr = Tools.calcThreeBezierPoints(lstart, end, lc1, lc2);
        bee.setMovePath(bzr);
        window.painter.addPaintElement(bee);
        window.fengwangs.push(bee);
        await sleep(this.otsp);
      }
    }

    resolve();
  }

  //右侧入场蜜蜂
  async beeEnter_rig(resolve, reject) {
    let top_gf = 150;
    let rstart = new Point(cavW - 10, -80);
    let rc1 = new Point(cavW - 60, 500);
    let rc2 = new Point(cavW / 2 - 400, 460);

    //工蜂出场
    for (let i = 0; i < 2; i++) {
      for (let j = 10; j > 0; j--) {
        let bee = new Foe_GongFeng();
        let ey = top_gf + i * (bee.height + 10);
        let ex = this.calcBaseLeft(10, bee.width) + (j - 1) * (bee.width + this.unitMargin_right);
        let end = new Point(ex, ey);
        bee.originalLocation = end;
        let bzr = Tools.calcThreeBezierPoints(rstart, end, rc1, rc2);
        bee.setMovePath(bzr);
        painter.addPaintElement(bee);
        window.gongfengs.push(bee);
        await sleep(this.otsp);
      }
    }
    let top_hwf = 110;
    //护卫峰出场
    for (let i = 0; i < 1; i++) {
      for (let j = 8; j > 0; j--) {
        let bee = new Foe_HuWeiFeng();
        let ey = top_hwf + i * (bee.height + 10);
        let ex = this.calcBaseLeft(8, bee.width) + (j - 1) * (bee.width + this.unitMargin_right);
        let end = new Point(ex, ey);
        bee.originalLocation = end;
        let bzr = Tools.calcThreeBezierPoints(rstart, end, rc1, rc2);
        bee.setMovePath(bzr);
        painter.addPaintElement(bee);
        window.hwfs.push(bee);
        await sleep(this.otsp);
      }
    }
    resolve();
  }
}
