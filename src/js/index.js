/**
 * (扩展) 绘制圆
 * @param {number} x x坐标
 * @param {number} y y坐标
 * @param {number} w 圆的半径
 */
CanvasRenderingContext2D.prototype.fillCircle = function (x, y, w) {
  this.beginPath();
  this.lineWidth = 1;
  this.arc(x, y, w, 0, 2 * Math.PI);
  this.stroke();
  this.fill();
  this.closePath();
};

/**
 * (扩展) 查找数组中是否包含指定的元素
 * @param {any} obj 要比较的对象
 * @param {boolean} fuzzy 使用==(true),===(false)
 * @returns {boolean}
 */
Array.prototype.has = function (obj, fuzzy = true) {
  for (let i = 0; i < this.length; i++) {
    if (fuzzy) {
      if (this[i] == obj) return true;
    } else {
      if (this[i] === obj) return true;
    }
  }
  return false;
};

/**
 * (扩展) 删除数组中第一个匹配的对象
 * @param {any} obj 要比较的对象
 * @returns {boolean} 是否找到并删除成功
 */
Array.prototype.remove = function (obj) {
  for (let i = 0; i < this.length; i++) {
    const elm = this[i];
    if (elm === obj) {
      this.splice(i, 1);
      return true;
    }
  }
  return false;
};

/**(扩展) 返回数组中的随机元素 */
Array.prototype.getRandElement = function () {
  if (this.length == 0) return null;
  return this[Tools.getRandInt(0, this.length - 1)];
};

//线程休眠函数
window.sleep = function (ms) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, ms);
  });
};

window.onload = function () {
  //全局画布对象
  window.gameCanvas = document.getElementsByTagName("canvas")[0];
  //全局绘制引擎
  window.painter = new Painter(gameCanvas);
  window.cavW = gameCanvas.width;
  window.cavH = gameCanvas.height;
  //控制器
  var controller = new Controller();
  window.heros = controller.heros;
  window.bingfengs = controller.bingfengs;
  window.hwfs = controller.hwfs;
  window.gongfengs = controller.gongfengs;
  window.fengwangs = controller.fengwangs;

  controller.startGame();
};
